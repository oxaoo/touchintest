package com.gmail.yol0317.touchintest;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import java.util.regex.Pattern;


public class MainActivity extends ActionBarActivity
{
    String m_log = "LOG";
    private static final String EDIT_TEXT_KEY = "EDIT_TEXT_KEY";
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        Log.d(m_log, "onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Button btn_scan = (Button) findViewById(R.id.btn_scan);
        final EditText et_url = (EditText) findViewById(R.id.et_url);

        btn_scan.setEnabled(false);
        btn_scan.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(MainActivity.this, ImageActivity.class);
                intent.putExtra("url", et_url.getText().toString());
                startActivity(intent);
            }
        });

        et_url.addTextChangedListener(new TextWatcher()
        {
            String urlMask = "(https?://)?[a-zA-Z_0-9\\-]+(\\.\\w[a-zA-Z_0-9\\-]+)+(/[#&\\n\\-=?\\+\\%/\\.\\w]+)?";
            Pattern patternUrl = Pattern.compile(urlMask);

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {

            }

            @Override
            public void afterTextChanged(Editable s)
            {
                Log.i(m_log, s.toString());
                if (patternUrl.matcher(s.toString()).matches())
                    btn_scan.setEnabled(true);
                else
                    btn_scan.setEnabled(false);
            }
        });


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings)
        {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onSaveInstanceState(Bundle savedInstanceState)
    {
        savedInstanceState.putString(EDIT_TEXT_KEY, ((EditText) findViewById(R.id.et_url)).getText().toString());
        super.onSaveInstanceState(savedInstanceState);
    }

    public void onRestoreInstanceState(Bundle savedInstanceState)
    {
        super.onRestoreInstanceState(savedInstanceState);
        String savedUrl = savedInstanceState.getString(EDIT_TEXT_KEY);
        ((EditText) findViewById(R.id.et_url)).setText(savedUrl);
    }
}
