package com.gmail.yol0317.touchintest;

/**
 * Created by Alexander on 23.07.2015.
 */
public class CGVItem
{
    private String image;
    private String title;

    public CGVItem()
    {
        super();
    }

    public String getImage()
    {
        return image;
    }

    public void setImage(String image)
    {
        this.image = image;
    }

    /*
    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }*/
}
