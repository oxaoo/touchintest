package com.gmail.yol0317.touchintest;

import android.app.Activity;
import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Alexander on 23.07.2015.
 */

public class CImgAdapter extends ArrayAdapter<CGVItem>
{
    private Context mContext;
    private int layoutResourceId;
    private ArrayList<CGVItem> mGridData = new ArrayList<CGVItem>();

    public CImgAdapter(Context mContext, int layoutResourceId, ArrayList<CGVItem> mGridData)
    {
        super(mContext, layoutResourceId, mGridData);
        this.layoutResourceId = layoutResourceId;
        this.mContext = mContext;
        this.mGridData = mGridData;
    }

    public void setGridData(ArrayList<CGVItem> mGridData)
    {
        this.mGridData = mGridData;
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        View row = convertView;
        ImageView imageView;

        if (row == null)
        {
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            imageView = (ImageView) row.findViewById(R.id.gvItemImg);
            row.setTag(imageView);
        }
        else
            imageView = (ImageView) row.getTag();

        CGVItem item = mGridData.get(position);
        Picasso.with(mContext).load(item.getImage()).into(imageView);
        return row;
    }
}
