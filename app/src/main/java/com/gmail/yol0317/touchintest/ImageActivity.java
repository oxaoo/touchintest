package com.gmail.yol0317.touchintest;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Looper;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.regex.Pattern;


public class ImageActivity extends ActionBarActivity
{
    String m_log = "LOG";

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image);

        final String url = getIntent().getStringExtra("url");
        setTitle(url);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        GridView imgGV = (GridView) findViewById(R.id.gvImg);
        ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar);

        progressBar.setVisibility(View.VISIBLE);
        ArrayList<CGVItem> itemsGV = new ArrayList<>();
        CImgAdapter imgAdapter = new CImgAdapter(this, R.layout.gridview_item, itemsGV);

        List<String> imgs = null;


        try
        {
            imgs = new CGetSourceImgTask().execute(url).get();
            correctImgUrl(imgs, url);
            for (String img: imgs)
                Log.i(m_log, "IMG: " + img);

        } catch (InterruptedException e)
        {
            Log.e(m_log, "Error during get a list of images: [" + e.toString() + "]");
            Toast.makeText(getApplicationContext(), "Error during get a list of images: [" + e.toString() + "]" + e.toString() + "]", Toast.LENGTH_LONG).show();
        } catch (ExecutionException e)
        {
            Log.e(m_log, "Error during get a list of images: [" + e.toString() + "]" + e.toString() + "]");
            Toast.makeText(getApplicationContext(), "Error during get a list of images: [" + e.toString() + "]" + e.toString() + "]", Toast.LENGTH_LONG).show();
        }


        imgGV.setAdapter(imgAdapter);

        for (String img: imgs)
        {
            CGVItem item = new CGVItem();
            item.setImage(img);
            itemsGV.add(item);
        }
        progressBar.setVisibility(View.GONE);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_image, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        /*
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings)
        {
            return true;
        }

        return super.onOptionsItemSelected(item);*/

        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        startActivityForResult(intent, 0);
        return true;
    }

    //поправить ссылки на изображения, если они относительные.
    public void correctImgUrl(List<String> imgs, String url)
    {
        if (!url.startsWith("http://") && !url.startsWith("https://") && url.startsWith("www."))
            url = "http://" + url;
        else if (!url.startsWith("http://") && !url.startsWith("https://") && !url.startsWith("www."))
            url = "http://www." + url;

        String urlMask = "https?://[a-zA-Z_0-9\\-]+(\\.\\w[a-zA-Z_0-9\\-]+)+(/[#&\\n\\-=?\\+\\%/\\.\\w]+)?";
        Pattern patternUrl = Pattern.compile(urlMask);

        for (int i = 0; i < imgs.size(); i++)
        {
            String img = imgs.get(i);
            if (img.startsWith("//")) //сокр. адрес.
                img = "http:" + img;


            if (img.startsWith("/")) //относительный адрес.
                img = img.substring(1);

            if (!patternUrl.matcher(img).matches())
                img = url + "/" + img;

            if (!imgs.contains(img))
                imgs.set(i, img);
        }
    }

    //загрузка изображения.
    public class CLoadImgTask extends AsyncTask<String, Void, Bitmap>
    {

        @Override
        protected Bitmap doInBackground(String... params)
        {
            String urlImg = params[0];
            Bitmap bitmap = null;
            try
            {
                InputStream in = new URL(urlImg).openStream();
                bitmap = BitmapFactory.decodeStream(in);
            } catch (Exception e)
            {
                Log.e(m_log, "Error during load image: [" + e.toString() + "]");
                Toast.makeText(getApplicationContext(), "Error during load image: [" + e.toString() + "]", Toast.LENGTH_LONG).show();
            }

            return bitmap;
        }
    }

    //загрузить список ресурсов изображений.
    public class CGetSourceImgTask extends AsyncTask<String, Void, List<String>>
    {
        boolean isException = false;

        @Override
        protected List<String> doInBackground(String... params)
        {
            String url = params[0];
            if (!url.startsWith("http://") && !url.startsWith("https://") && url.startsWith("www."))
                url = "http://" + url;
            else if (!url.startsWith("http://") && !url.startsWith("https://") && !url.startsWith("www."))
                url = "http://www." + url;

            //Log.i(m_log, "URL: " + url);

            List<String> imgs = new ArrayList<String>();
            try
            {
                Document doc = Jsoup.connect(url).get();
                for (Element img : doc.getElementsByTag("img"))
                    imgs.add(img.attr("src"));
                //Log.i(m_log, "IMG: " + doc.getElementsByTag("img").toString());
            } catch (IOException e)
            {
                Log.e(m_log, "Error during download code of page: [" + e.toString() + "]");
                isException = true;
            }

            return imgs;
        }

        protected void onPostExecute(List<String> result)
        {
            if (isException)
                Toast.makeText(getApplicationContext(), "Error during download code of page.", Toast.LENGTH_LONG).show();
        }

    }
}
